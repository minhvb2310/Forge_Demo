import AboutDlg from '../../../components/Dialogs/AboutDlg'
import Coverflow from 'react-coverflow'
import { IndexLink, Link } from 'react-router'
import React from 'react'

import './HomeView.scss'
import { Tab } from 'semantic-ui-react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Visibility,
  Card,
} from 'semantic-ui-react'
// const options = ;
 
const events = {
    onDragged: function(event) {console.log('dragged!')},
    onChanged: function(event) {console.log('changed!')}
};
class HomeView extends React.Component {
  /// //////////////////////////////////////////////////////////////
  //
  //
  /// //////////////////////////////////////////////////////////////
  constructor () {
    super()

    this.state = {
      aboutOpen: false,
      models_peripheral: [
        {
          path: 'resources/models/seat/seat.svf',
          thumbnail: 'resources/models/seat/thumbnail.png',
          name: 'Seat car'
        },
        {
          path: 'resources/models/shaver/0.svf',
          thumbnail: 'resources/models/shaver/thumbnail.png',
          name: 'Shaver - An electric razor'
        },
        {
          path: 'resources/models/engine/0.svf',
          thumbnail: 'resources/models/engine/thumbnail.png',
          name: 'Engine'
        },
        {
          path: 'resources/models/LHC/0.svf',
          thumbnail: 'resources/models/LHC/lhc.png',
          name: 'Large Hadron Collider & Engine'
        }
      ],

      models_building: [
        {
          path: 'resources/models/buildings/building.3DS.svf',
          thumbnail: 'resources/models/buildings/building.3DS.svf.png',
          name: 'High-rise flats'
        },
        {
          path: 'resources/models/buildings_2/0.svf',
          thumbnail: 'resources/models/buildings_2/0.svf.png',
          name: 'Office'
        },
        {
          path: 'resources/models/buildings_3/0.svf',
          thumbnail: 'resources/models/buildings_3/0.svf.png',
          name: 'Empty shop'
        },
        {
          path: 'resources/models/brick_house/{3D}.svf',
          thumbnail: 'resources/models/brick_house/{3D}.svf.png',
          name: 'Brick house'
        },
        {
          urn: 'dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bWluaHZ1MjMvR2F6ZWJvLXJvdW5kLklHUw',
          thumbnail: 'resources/models/thumbnails/gazebo-round.JPG',
          name: 'Gazebo Round'
        }
      ],

      models_electronic: [
        {
          path: 'resources/models/electronic_1/0.svf',
          thumbnail: 'resources/models/electronic_1/0.svf.png',
          name: 'Cyrille Caractress'
        },
        {
          path: 'resources/models/motherboard/Motherboard.svf',
          thumbnail: 'resources/models/motherboard/Motherboard.svf.png',
          name: 'Motherboard'
        },
        {
          path: 'resources/models/odroid/Odroid-XU4.svf',
          thumbnail: 'resources/models/odroid/Odroid-XU4.svf.png',
          name: 'Odroid XU4'
        },
        {
          path: 'resources/models/duetwifi/DuetWifiRev4.svf',
          thumbnail: 'resources/models/duetwifi/DuetWifiRev4.svf.png',
          name: 'Duet Wifi'
        }
      ]
    }
    this.openAboutDlg = this.openAboutDlg.bind(this)
  }
  openAboutDlg () {
    this.setState(Object.assign({}, this.state, {
      aboutOpen: true
    }))
  }
  hideFixedMenu = () => this.setState({ visible: false })
  showFixedMenu = () => this.setState({ visible: true })
  /// //////////////////////////////////////////////////////////////
  //
  //      Funtions to render models
  /// //////////////////////////////////////////////////////////////
  renderTab (models = []) {
    return (
      <div className='models inner-models'>
        <div className='content responsive-grid'>
          {
            models.map((model, idx) => {
              return (
                <Link key={idx} to={`/viewer?${model.path ? 'path=' + model.path : 'urn=' + model.urn}`}>
                  <figure>
                    <img src={model.thumbnail || 'resources/img/forge.png'} />
                    <figcaption>
                      {model.name}
                    </figcaption>
                  </figure>
                </Link>)
            })
          }
        </div>
      </div>
    )
  }
  /// //////////////////////////////////////////////////////////////
  //
  //
  /// //////////////////////////////////////////////////////////////
  render () {
    const FixedMenu = () => (
      <Menu fixed='top' size='large'>
        <Container>
          <Menu.Item as='a' active>Home</Menu.Item>
          <Menu.Item as='a' onClick={() => {this.openAboutDlg()}}>About...</Menu.Item>
          <Menu.Menu position='right'>
            <Menu.Item as='a'><img src="/resources/img/bhtech-logo.jpg" alt=""/></Menu.Item>
          </Menu.Menu>
        </Container>
      </Menu>
    )
    const { visible } = this.state
    // const panes = [
    //   { menuItem: { key: 'buildings', icon: 'building', content: 'Buildings' }, render: () => this.renderTab(this.state.models_building) },
    //   { menuItem: { key: 'electrons', icon: 'plug', content: 'Electrons' }, render: () => this.renderTab(this.state.models_electronic) },
    //   { menuItem: { key: 'peripheral', icon: 'disk outline', content: 'Peripheral units' }, render: () => this.renderTab(this.state.models_peripheral) }
    // ]
    const panes = [
      { menuItem: 'Tab 1', render: () => <Tab.Pane attached={false}>Tab 1 Content</Tab.Pane> },
      { menuItem: 'Tab 2', render: () => <Tab.Pane attached={false}>Tab 2 Content</Tab.Pane> },
      { menuItem: 'Tab 3', render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane> },
    ]
    const test = function () {console.log('kfjdslj')}
    return (
      <div>
        <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css' />
        { visible ? <FixedMenu /> : null }

        <Visibility
          onBottomPassed={this.showFixedMenu}
          onBottomVisible={this.hideFixedMenu}
          once={false}
        >
          <Segment
            inverted
            textAlign='center'
            style={{ minHeight: 700, padding: '1em 0em' }}
            vertical
          >
            <Container>
              <Menu inverted pointing secondary size='large'>
                <Menu.Item as='a'><img src="/resources/img/bhtech-logo.jpg" alt=""/></Menu.Item>
                <Menu.Menu position='right'>
                  <Menu.Item as='a' active>Home</Menu.Item>
                  <Menu.Item as='a' onClick={() => {this.openAboutDlg()}}>About...</Menu.Item>
                </Menu.Menu>
              </Menu>
            </Container>
            <Container text>
              <Header
                as='h1'
                content='Imagine-a-Company'
                inverted
                style={{ fontSize: '4em', fontWeight: 'normal', marginBottom: 0, marginTop: '3em' }}
              />
              <Header
                as='h2'
                content='Do whatever you want when you want to.'
                inverted
                style={{ fontSize: '1.7em', fontWeight: 'normal' }}
              />
              <Button primary size='huge'>
                Get Started
                <Icon name='right arrow' />
              </Button>
            </Container>
          </Segment>
        </Visibility>

        <Segment style={{ padding: '8em 0em' }} vertical>
          <Grid container stackable verticalAlign='middle'>
            <Grid.Row>
              <Header textAlign='center' as='h3' style={{ fontSize: '2em', width: '100%' }}>We Help Companies and Companions</Header>
              <Coverflow
                width={960}
                height={480}
                displayQuantityOfSide={2}
                navigation={false}
                enableHeading={false}
                >
                <Card>
                  <Image src='resources/models/thumbnails/gazebo-round.JPG' />
                  <Card.Content>
                    <Card.Header>
                      Matthew
                    </Card.Header>
                    <Card.Meta>
                      <span className='date'>
                        Joined in 2015
                      </span>
                    </Card.Meta>
                    <Card.Description>
                      Matthew is a musician living in Nashville.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <a>
                      <Icon name='user' />
                      22 Friends
                    </a>
                  </Card.Content>
                </Card>
                <Card>
                  <Image src='resources/models/thumbnails/gazebo-round.JPG' />
                  <Card.Content>
                    <Card.Header>
                      Matthew
                    </Card.Header>
                    <Card.Meta>
                      <span className='date'>
                        Joined in 2015
                      </span>
                    </Card.Meta>
                    <Card.Description>
                      Matthew is a musician living in Nashville.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <a>
                      <Icon name='user' />
                      22 Friends
                    </a>
                  </Card.Content>
                </Card>
                <Card>
                  <Image src='resources/models/thumbnails/gazebo-round.JPG' />
                  <Card.Content>
                    <Card.Header>
                      Matthew
                    </Card.Header>
                    <Card.Meta>
                      <span className='date'>
                        Joined in 2015
                      </span>
                    </Card.Meta>
                    <Card.Description>
                      Matthew is a musician living in Nashville.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <a>
                      <Icon name='user' />
                      22 Friends
                    </a>
                  </Card.Content>
                </Card>
                <Card data-action='/viewer?path=resources/models/duetwifi/DuetWifiRev4.svf'>
                  <Image src='resources/models/thumbnails/gazebo-round.JPG' />
                  <Card.Content>
                    <Card.Header>
                      Matthew
                    </Card.Header>
                    <Card.Meta>
                      <span className='date'>
                        Joined in 2015
                      </span>
                    </Card.Meta>
                    <Card.Description>
                      Matthew is a musician living in Nashville.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <a>
                      <Icon name='user' />
                      22 Friends
                    </a>
                  </Card.Content>
                </Card>
                <Card>
                  <Image src='resources/models/thumbnails/gazebo-round.JPG' />
                  <Card.Content>
                    <Card.Header>
                      Matthew
                    </Card.Header>
                    <Card.Meta>
                      <span className='date'>
                        Joined in 2015
                      </span>
                    </Card.Meta>
                    <Card.Description>
                      Matthew is a musician living in Nashville.
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <a onClick={() => console.log('kdjflsdj')} href='http://google.com.vn'>
                      <Icon name='user' />
                      22 Friends
                    </a>
                  </Card.Content>
                </Card>
              </Coverflow>
              {/* <Grid.Column width={8}>
                <Header as='h3' style={{ fontSize: '2em' }}>We Help Companies and Companions</Header>
                <p style={{ fontSize: '1.33em' }}>
                  We can give your company superpowers to do things that they never thought possible. Let us delight
                  your customers and empower your needs... through pure data analytics.
                </p>
                <Header as='h3' style={{ fontSize: '2em' }}>We Make Bananas That Can Dance</Header>
                <p style={{ fontSize: '1.33em' }}>
                  Yes that's right, you thought it was the stuff of dreams, but even bananas can be bioengineered.
                </p>
              </Grid.Column>
              <Grid.Column floated='right' width={6}>
                <Image
                  bordered
                  size='large'
                  src='/assets/images/wireframe/white-image.png'
                />
              </Grid.Column> */}
            </Grid.Row>
            <Grid.Row>
              <Grid.Column textAlign='center'>
                <Button size='huge'>Check Them Out</Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment style={{ padding: '0em' }} vertical>
          <Grid celled='internally' columns='equal' stackable>
            <Grid.Row textAlign='center'>
              <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
                <Header as='h3' style={{ fontSize: '2em' }}>"What a Company"</Header>
                <p style={{ fontSize: '1.33em' }}>That is what they all say about us</p>
              </Grid.Column>
              <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
                <Header as='h3' style={{ fontSize: '2em' }}>"I shouldn't have gone with their competitor."</Header>
                <p style={{ fontSize: '1.33em' }}>
                  <Image avatar src='/assets/images/avatar/large/nan.jpg' />
                  <b>Nan</b> Chief Fun Officer Acme Toys
                </p>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment style={{ padding: '8em 0em' }} vertical>
          <Container fluid>
            <Header as='h3' style={{ fontSize: '2em' }}>Breaking The Grid, Grabs Your Attention</Header>
            <p style={{ fontSize: '1.33em' }}>
              Instead of focusing on content creation and hard work, we have learned how to master the art of doing
              nothing by providing massive amounts of whitespace and generic content that can seem massive, monolithic
              and worth your attention.
            </p>
            <Button as='a' size='large'>Read More</Button>
            <Divider
              as='h4'
              className='header'
              horizontal
              style={{ margin: '3em 0em', textTransform: 'uppercase' }}
            >
              <a href='#'>Case Studies</a>
            </Divider>
            <Header as='h3' style={{ fontSize: '2em' }}>Did We Tell You About Our Bananas?</Header>
            <p style={{ fontSize: '1.33em' }}>
              Yes I know you probably disregarded the earlier boasts as non-sequitur filler content, but its really
              true.
              It took years of gene splicing and combinatory DNA research, but our bananas can really dance.
            </p>
            <Button as='a' size='large'>I'm Still Quite Interested</Button>
          </Container>
        </Segment>
        <Segment inverted vertical style={{ padding: '5em 0em' }}>
          <Container>
            <Grid divided inverted stackable>
              <Grid.Row>
                <Grid.Column width={3}>
                  <Header inverted as='h4' content='About' />
                  <List link inverted>
                    <List.Item as='a'>Sitemap</List.Item>
                    <List.Item as='a'>Contact Us</List.Item>
                    <List.Item as='a'>Religious Ceremonies</List.Item>
                    <List.Item as='a'>Gazebo Plans</List.Item>
                  </List>
                </Grid.Column>
                <Grid.Column width={3}>
                  <Header inverted as='h4' content='Services' />
                  <List link inverted>
                    <List.Item as='a'>Banana Pre-Order</List.Item>
                    <List.Item as='a'>DNA FAQ</List.Item>
                    <List.Item as='a'>How To Access</List.Item>
                    <List.Item as='a'>Favorite X-Men</List.Item>
                  </List>
                </Grid.Column>
                <Grid.Column width={7}>
                  <Header as='h4' inverted>Footer Header</Header>
                  <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </Segment>
        <AboutDlg
          close={()=>{ this.setState(Object.assign({}, this.state, {
            aboutOpen: false
          }))}}
          open={this.state.aboutOpen}
        />
      </div>
      // <div className='home '>
      //   <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css' />
      //   <div className='models' style={{overflow: 'hidden'}}>
      //     <div style={{borderBottom: 'none'}} className='title'>
      //       <Tab panes={panes} />
      //     </div>
      //   </div>
      //   <div className='models second-panel' style={{overflow: 'hidden'}}>
      //     <div style={{borderBottom: 'none'}} className='title'>
      //       <Tab panes={panes} />
      //     </div>
      //   </div>
      //   <img className='logo-hero' src='resources/img/hero-banner.jpg' />
      // </div>
    )
  }
}

export default HomeView
