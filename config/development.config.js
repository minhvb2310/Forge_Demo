
/// //////////////////////////////////////////////////////////////////
// DEVELOPMENT configuration
//
/// //////////////////////////////////////////////////////////////////
const config = {

  client: {
    host: 'http://localhost',
    env: 'development',
    port: 3000
  },

  forge: {

    oauth: {
      clientSecret: 'uSM3sQxUSrfGz1QW',
      clientId: 'uLekEKkZI1AEjN8GbU7AwgRY0h1e3g35',
      scope: [
        'data:read',
        'data:create',
        'data:write',
        'bucket:read',
        'bucket:create'
      ]
    },

    viewer: {
      viewer3D: 'https://developer.api.autodesk.com/viewingservice/v1/viewers/viewer3D.js?v=v2.13',
      threeJS: 'https://developer.api.autodesk.com/viewingservice/v1/viewers/three.js?v=v2.13',
      style: 'https://developer.api.autodesk.com/viewingservice/v1/viewers/style.css?v=v2.13',
      abc: 'https://developer.api.autodesk.com/viewingservice/v1/viewers/minh_test.css?v=v2.13'
    }
  }
}

module.exports = config
